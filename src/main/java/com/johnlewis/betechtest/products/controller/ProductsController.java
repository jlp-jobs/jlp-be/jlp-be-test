package com.johnlewis.betechtest.products.controller;

import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.service.DressesService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping( "/products" )
public class ProductsController {

    private final DressesService dressesService;

    @Autowired
    ProductsController( DressesService dressesService )
    {
        this.dressesService = dressesService;
    }

    @GetMapping( value = "/dresses", produces = MediaType.APPLICATION_JSON_VALUE )
    @ApiOperation(value = "Endpoint to obtain dress information")
    public ResponseEntity<List<Product>> getDresses( ) {
        return ResponseEntity.ok(this.dressesService.getDresses());
    }
}
