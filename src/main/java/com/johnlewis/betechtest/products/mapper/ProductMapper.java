package com.johnlewis.betechtest.products.mapper;

import com.johnlewis.betechtest.products.response.Price;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.response.ProductDto;
import com.johnlewis.betechtest.products.response.ReductionHistoryDto;
import com.johnlewis.betechtest.products.response.VariantPriceRangeDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    @Mapping(target = "productId", source = "productId")
    @Mapping(target = "title", source = "title")
    @Mapping(target = "price", source = "variantPriceRange")
    Product toProduct(ProductDto productDto);

    default Price toPrice(VariantPriceRangeDto variantPriceRangeDto) {
        if (variantPriceRangeDto == null || variantPriceRangeDto.getDisplay() == null) {
            return null;
        }
        String was = null;
        String then = null;
        String now = variantPriceRangeDto.getDisplay().getMax();

        List<ReductionHistoryDto> reductionHistories = variantPriceRangeDto.getReductionHistory();
        // It's possible there has been more than 2 reductions, but we will just take the original price and the first reduction for was and then.
        // Products can have a min and max price in the instance there is a range for a particular product
        // (e.g. age 9 dress may be cheaper than an age 11 dress)
        // For simplicity we will assume the max and min prices are the same and there is no range.
        if (reductionHistories != null && !reductionHistories.isEmpty()) {
            List<ReductionHistoryDto> sortedReductionHistories = reductionHistories.stream()
                    .sorted(Comparator.comparingInt(ReductionHistoryDto::getChronology))
                    .collect(Collectors.toList());
            was = sortedReductionHistories.get(0).getDisplay().getMax();
            if (sortedReductionHistories.size() > 1) {
                then = sortedReductionHistories.get(1).getDisplay().getMax();
            }
        }
        return new Price(was, then, now);
    }
}
