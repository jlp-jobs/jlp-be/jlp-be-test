package com.johnlewis.betechtest.products.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Product {
    private final String productId;
    private final String title;
    private final Price price;
}
