package com.johnlewis.betechtest.products.config;

import com.johnlewis.betechtest.products.mapper.ProductMapper;
import com.johnlewis.betechtest.products.mapper.ProductMapperImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductMapperConfig {

    @Bean
    ProductMapper productMapper() {
        return new ProductMapperImpl();
    }
}
