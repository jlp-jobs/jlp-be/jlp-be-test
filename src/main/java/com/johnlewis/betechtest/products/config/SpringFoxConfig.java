package com.johnlewis.betechtest.products.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;

@Configuration
public class SpringFoxConfig {
    public static final String TERMS_OF_SERVICE_URL = "urn:tos";
    public static final String LICENSE = "Apache 2.0";
    public static final String LICENSE_URL = "http://www.apache.org/licenses/LICENSE-2.0";

    @Value("${swagger.title}")
    private String title;
    @Value("${swagger.description}")
    private String description;
    @Value("${swagger.version}")
    private String version;

    @Bean
    public Docket api() {

        ApiInfo info = new ApiInfo(
            title,
            description,
            version,
            TERMS_OF_SERVICE_URL,
            ApiInfo.DEFAULT_CONTACT,
            LICENSE,
            LICENSE_URL,
            new ArrayList<>());

        return new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(info)
            .select()
            .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
            .paths(PathSelectors.any())
            .build();
    }
}