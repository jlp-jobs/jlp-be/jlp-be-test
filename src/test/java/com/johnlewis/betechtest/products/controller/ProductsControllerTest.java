package com.johnlewis.betechtest.products.controller;

import com.johnlewis.betechtest.products.response.Price;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.service.DressesService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class ProductsControllerTest {

    private static final String PRODUCT_ID = "productId";
    private static final String TITLE = "title";
    private static final String WAS = "£15.00";
    private static final String THEN = "£10.00";
    private static final String NOW = "£5.00";
    private static final Price PRICE = new Price(WAS, THEN, NOW);

    private final DressesService dressesService = mock(DressesService.class);
    private final ProductsController productsController = new ProductsController(dressesService);

    @AfterEach
    void after() {
        verifyNoMoreInteractions(dressesService);
    }

    @Test
    void shouldReturnDressInformation() {
        List<Product> expectedProductList = List.of(new Product(PRODUCT_ID, TITLE, PRICE));
        given(dressesService.getDresses()).willReturn(expectedProductList);
        ResponseEntity<List<Product>> result = productsController.getDresses();

        verify(dressesService).getDresses();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(expectedProductList);
    }
}