# JLP Back-end Technical Test - Dresses

## About
This application exposes an API to return dresses, using data from the John Lewis product search API.  This is a Java gradle project that uses 
[SpringBoot](https://spring.io/projects/spring-boot), [MapStruct](https://mapstruct.org/) and [Lombok](https://projectlombok.org/).  Don't worry
if you haven't used this framework or libraries before - use the existing implementation and tests as a guide.

## Starting the application
The application can be started using Gradle:
```shell
./gradlew bootRun
```
Once started the application will return product information from the product search API via this endpoint:

[`http://localhost:8080/products/dresses`](http://localhost:8080/products/dresses)

The Open API documentation for the endpoint can be found here:

[`http://localhost:8080/swagger-ui/`](http://localhost:8080/swagger-ui/)

## Your Brief
We would like you to add a *new* restful endpoint that only returns dresses reduced in price, with a response body as follows:
```
[
  {
    "productId": "5727625",
    "title": "ANYDAY John Lewis & Partners Kids' Tiered Jersey Dress",
    "nowPrice": "£4.50",
    "priceLabel": "50% off - now £4.50"
  }
]
```
Pay particular attention to:

### nowPrice (String)
Current price represented as a string, including the currency, e.g. “£1.75”.
* For values of £10 or greater that are integers, show a price without decimal places, e.g. "£10", "£200"
* Otherwise, show price to 2 decimal places, e.g. "£9.00", “£10.43”

### priceLabel (String)
Highlights the discount that has been applied to this product. The format returned depends on the value of (optional) query parameter `labelType`

| labelType | format of priceLabel                                                                         |
| ------ |----------------------------------------------------------------------------------------------|
| `ShowWasNow` | “Was £x.xx, now £y.yy”                                                                       |
| `ShowWasThenNow` | “Was £x.xx, then £y.yy, now £z.zz”. If there is no value in price.then, display as per ShowWasNow. |
| `ShowPercDscount` | “x% off - now £y.yy”. This should be the percentage reduction from the `was` price.          |

* Prices within the label should be formatted as per nowPrice above.
* If `labelType` is not specified or has any other value, display as per ShowWasNow.

### Trapping errors with the data provided by the API
If there is invalid data returned from the internal api providing the dresses data, prefer an empty string / assume a value of zero / skip the record rather than creating complex error handling.

## Things we're looking for

- Unit tests are important. We’d like to see a TDD approach to writing the app.
- Put all your assumptions, notes, instructions and improvement suggestions into your README.md.
- We'll be assessing your coding style and how you've approached this task.
- We don't expect you to spend too long on this, as a guide 3 hours is usually enough.

## Ready to begin ?

- Firstly register a GitLab user and login to GitLab.
- When logged in, go to the top of this repository and click the `Request Access` link.  That lets us know who you are in GitLab.
- Next click the `Fork` link, top right.
- Make sure the Visibility Level is set to __PRIVATE__
- Click `Fork project`
- Then clone your new fork and start coding.  

## Submitting your code

- When complete, please grant `developer` access to the `jlp-test-review/candidate` GitLab group.
- Drop us an email to let us know you've finished.

 
